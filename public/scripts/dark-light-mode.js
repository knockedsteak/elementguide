 // Wait for document to load
 document.addEventListener("DOMContentLoaded", function(event) {
    console.log("reached");
    document.documentElement.setAttribute("data-theme", "mode=light");

    // Get our button switcher
    var themeSwitcher = document.getElementById("switch");

    // When our button gets clicked
    themeSwitcher.onclick = function() {
      // Get the current selected theme, on the first run
      // it should be `light`
      var currentTheme = document.documentElement.getAttribute("data-theme");

      // Switch between `dark` and `light`
      var switchToTheme = currentTheme === "mode=dark" ? "mode=light" : "mode=dark";

      document.cookie = switchToTheme + " ;path=/";

      // Set our current theme to the new one
      document.documentElement.setAttribute("data-theme", switchToTheme);
    }
  });
